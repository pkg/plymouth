From 65d854172dffce2cb2c64dacba325184022fd10e Mon Sep 17 00:00:00 2001
From: Sjoerd Simons <sjoerd.simons@collabora.co.uk>
Date: Mon, 8 Dec 2014 11:50:47 +0100
Subject: [PATCH 14/17] script: Don't draw backgrounds if they're obscured

When drawing sprites it's a waste to draw the background if the target
area will be obscured by the sprit to draw. Optimize for the common case
where only one sprite is being drawn by peeking at the first item in the
sprite list to check if it is opaque and fills the area.
---
 src/plugins/splash/script/script-lib-sprite.c | 114 ++++++++++++++++----------
 1 file changed, 71 insertions(+), 43 deletions(-)

Index: plymouth-0.9.2/src/plugins/splash/script/script-lib-sprite.c
===================================================================
--- plymouth-0.9.2.orig/src/plugins/splash/script/script-lib-sprite.c	2016-05-04 16:07:33.043673241 +0200
+++ plymouth-0.9.2/src/plugins/splash/script/script-lib-sprite.c	2016-05-04 16:10:55.702239289 +0200
@@ -416,57 +416,86 @@
         return script_return_obj_null ();
 }
 
-static void script_lib_sprite_draw_area (script_lib_display_t *display,
-                                         ply_pixel_buffer_t   *pixel_buffer,
-                                         int                   x,
-                                         int                   y,
-                                         int                   width,
-                                         int                   height)
+static void script_lib_draw_brackground (ply_pixel_buffer_t *pixel_buffer,
+                                         ply_rectangle_t *clip_area,
+                                         script_lib_sprite_data_t *data)
 {
-        ply_rectangle_t clip_area;
-        ply_list_node_t *node;
-        script_lib_sprite_data_t *data = display->data;
-
-        clip_area.x = x;
-        clip_area.y = y;
-        clip_area.width = width;
-        clip_area.height = height;
-
         if (data->background_color_start == data->background_color_end) {
                 ply_pixel_buffer_fill_with_hex_color (pixel_buffer,
-                                                      &clip_area,
+                                                      clip_area,
                                                       data->background_color_start);
         } else {
                 ply_pixel_buffer_fill_with_gradient (pixel_buffer,
-                                                     &clip_area,
+                                                     clip_area,
                                                      data->background_color_start,
                                                      data->background_color_end);
         }
-        for (node = ply_list_get_first_node (data->sprite_list);
-             node;
-             node = ply_list_get_next_node (data->sprite_list, node)) {
-                sprite_t *sprite = ply_list_node_get_data (node);
-                int position_x, position_y;
-
-                if (!sprite->image) continue;
-                if (sprite->remove_me) continue;
-                if (sprite->opacity < 0.011) continue;
-
-                position_x = sprite->x - display->x;
-                position_y = sprite->y - display->y;
+}
 
-                if (position_x >= (x + width)) continue;
-                if (position_y >= (y + height)) continue;
+static void script_lib_sprite_draw_area (script_lib_display_t *display,
+                                         ply_pixel_buffer_t   *pixel_buffer,
+                                         int                   x,
+                                         int                   y,
+                                         int                   width,
+                                         int                   height)
+{
+              ply_rectangle_t clip_area;
+              ply_list_node_t *node;
+              sprite_t *sprite;
+              script_lib_sprite_data_t *data = display->data;
+      
+              clip_area.x = x;
+              clip_area.y = y;
+              clip_area.width = width;
+              clip_area.height = height;
+      
+      
+              node = ply_list_get_first_node (data->sprite_list);
+              sprite = ply_list_node_get_data (node);
+      
+              /* Check If the first sprite should be rendered opaque */
+              if (sprite->image && !sprite->remove_me &&
+                  ply_pixel_buffer_is_opaque (sprite->image) && sprite->opacity == 1.0)  {
+                      int position_x = sprite->x - display->x;
+                      int position_y = sprite->y - display->y;
+      
+                      /* In that case only draw the background if the sprite doesn't
+                       * cover the complete area */
+                      if (position_x > x || position_y > y ||
+                          (ply_pixel_buffer_get_width (sprite->image) + position_x) < (x + width) ||
+                          (ply_pixel_buffer_get_height (sprite->image) + position_y) < (y + height))
+                              script_lib_draw_brackground (pixel_buffer, &clip_area, data);
+              } else {
+                      script_lib_draw_brackground (pixel_buffer, &clip_area, data);
+              }
+
+              for (node = ply_list_get_first_node (data->sprite_list);
+                   node;
+                   node = ply_list_get_next_node (data->sprite_list, node)) {
+                      int position_x, position_y;
+      
+                      sprite = ply_list_node_get_data (node);
+      
+                      if (!sprite->image) continue;
+                      if (sprite->remove_me) continue;
+                      if (sprite->opacity < 0.011) continue;
+      
+                      position_x = sprite->x - display->x;
+                      position_y = sprite->y - display->y;
+      
+                      if (position_x >= (x + width)) continue;
+                      if (position_y >= (y + height)) continue;
+      
+                      if ((position_x + (int) ply_pixel_buffer_get_width (sprite->image)) <= x) continue;
+                      if ((position_y + (int) ply_pixel_buffer_get_height (sprite->image)) <= y) continue;
+                      ply_pixel_buffer_fill_with_buffer_at_opacity_with_clip (pixel_buffer,
+                                                                              sprite->image,
+                                                                              position_x,
+                                                                              position_y,
+                                                                              &clip_area,
+                                                                              sprite->opacity);
+              }
 
-                if ((position_x + (int) ply_pixel_buffer_get_width (sprite->image)) <= x) continue;
-                if ((position_y + (int) ply_pixel_buffer_get_height (sprite->image)) <= y) continue;
-                ply_pixel_buffer_fill_with_buffer_at_opacity_with_clip (pixel_buffer,
-                                                                        sprite->image,
-                                                                        position_x,
-                                                                        position_y,
-                                                                        &clip_area,
-                                                                        sprite->opacity);
-        }
 }
 
 static void
