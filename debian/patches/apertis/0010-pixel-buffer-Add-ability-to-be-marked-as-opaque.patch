From ed65bb339c3b7e841cc3359985a3c80ee80282b1 Mon Sep 17 00:00:00 2001
From: Sjoerd Simons <sjoerd.simons@collabora.co.uk>
Date: Mon, 8 Dec 2014 11:39:39 +0100
Subject: [PATCH 10/17] pixel-buffer: Add ability to be marked as opaque

Pixel buffers contain ARGB32 data. Add functionality to mark a buffer as
containing only fully opaque pixels. This functionality can be used by
rendering functions to be able to optimize drawing of such buffers.
---
 src/libply-splash-core/ply-pixel-buffer.c | 23 +++++++++++++++++++++++
 src/libply-splash-core/ply-pixel-buffer.h |  3 +++
 2 files changed, 26 insertions(+)

Index: plymouth-0.9.2/src/libply-splash-core/ply-pixel-buffer.c
===================================================================
--- plymouth-0.9.2.orig/src/libply-splash-core/ply-pixel-buffer.c	2016-05-04 15:57:32.840075085 +0200
+++ plymouth-0.9.2/src/libply-splash-core/ply-pixel-buffer.c	2016-05-04 15:59:53.457853979 +0200
@@ -37,6 +37,8 @@
 #include <stdlib.h>
 #include <unistd.h>
 
+#define ALPHA_MASK 0xff000000
+
 struct _ply_pixel_buffer
 {
         uint32_t       *bytes;
@@ -45,6 +47,7 @@
         ply_list_t     *clip_areas;
 
         ply_region_t   *updated_areas;
+        bool opaque;
 };
 
 static inline void ply_pixel_buffer_blend_value_at_pixel (ply_pixel_buffer_t *buffer,
@@ -198,6 +201,11 @@
 
         ply_pixel_buffer_crop_area_to_clip_area (buffer, fill_area, &cropped_area);
 
+        if (fill_area == &buffer->area && ((pixel_value & ALPHA_MASK) == ALPHA_MASK) ) {
+          /* full area, becomes opaque if blending opaque values */
+          buffer->opaque = true;
+        }
+
         for (row = cropped_area.y; row < cropped_area.y + cropped_area.height; row++) {
                 for (column = cropped_area.x; column < cropped_area.x + cropped_area.width; column++) {
                         ply_pixel_buffer_blend_value_at_pixel (buffer,
@@ -244,6 +252,7 @@
 
         buffer->clip_areas = ply_list_new ();
         ply_pixel_buffer_push_clip_area (buffer, &buffer->area);
+        buffer->opaque = false;
 
         return buffer;
 }
@@ -295,6 +304,20 @@
         return buffer->area.height;
 }
 
+bool
+ply_pixel_buffer_is_opaque (ply_pixel_buffer_t *buffer)
+{
+        assert (buffer != NULL);
+        return buffer->opaque;
+}
+
+void
+ply_pixel_buffer_set_opaque (ply_pixel_buffer_t *buffer, bool opaque)
+{
+        assert (buffer != NULL);
+        buffer->opaque = opaque;
+}
+
 ply_region_t *
 ply_pixel_buffer_get_updated_areas (ply_pixel_buffer_t *buffer)
 {
Index: plymouth-0.9.2/src/libply-splash-core/ply-pixel-buffer.h
===================================================================
--- plymouth-0.9.2.orig/src/libply-splash-core/ply-pixel-buffer.h	2016-05-04 15:57:32.840075085 +0200
+++ plymouth-0.9.2/src/libply-splash-core/ply-pixel-buffer.h	2016-05-04 15:57:32.836075036 +0200
@@ -47,6 +47,9 @@
 unsigned long ply_pixel_buffer_get_width (ply_pixel_buffer_t *buffer);
 unsigned long ply_pixel_buffer_get_height (ply_pixel_buffer_t *buffer);
 
+bool ply_pixel_buffer_is_opaque (ply_pixel_buffer_t *buffer);
+void ply_pixel_buffer_set_opaque (ply_pixel_buffer_t *buffer, bool opaque);
+
 ply_region_t *ply_pixel_buffer_get_updated_areas (ply_pixel_buffer_t *buffer);
 
 void ply_pixel_buffer_fill_with_color (ply_pixel_buffer_t *buffer,
