Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: acinclude.m4
Copyright: 2003-2005, Thomas Vander Stichele <thomas at apestaart dot org>
License: FSFAP

Files: aclocal.m4
Copyright: 1996-2020, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2+) with Autoconf-data exception

Files: build-tools/*
Copyright: 1996-2020, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build-tools/config.rpath
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: build-tools/install-sh
Copyright: 1994, X Consortium
License: X11

Files: build-tools/ltmain.sh
Copyright: 1996-2015, Free Software Foundation, Inc.
License: (GPL-2+ and/or GPL-3+) with Libtool exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: 2009-2014 Daniel Baumann <mail@daniel-baumann.ch>
License: GPL-2+

Files: debian/patches/apertis/0001-WIP-gstreamer-Add-a-GStreamer-splash-plugin.patch
 debian/patches/apertis/0006-Refactor-gstreamer-support.patch
Copyright: 2013, Collabora Ltd.
 2009, Red Hat, Inc.
License: GPL-2+

Files: debian/patches/apertis/0007-Add-Gstreamer-support-to-the-script-plugin.patch
Copyright: 2013, Sjoerd Simons <sjoerd.simons@collabora.co.uk>
 2009, Charlie Brej <cbrej@cs.man.ac.uk>
License: GPL-2+

Files: m4/*
Copyright: 1996-2020, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/gettext.m4
 m4/intlmacosx.m4
 m4/po.m4
 m4/progtest.m4
Copyright: 1995-2020, Free Software Foundation, Inc.
License: FSFULLR and/or GPL and/or LGPL

Files: m4/iconv.m4
Copyright: 2000-2002, 2007-2014, 2016-2020, Free Software Foundation
License: FSFULLR

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2) with Libtool exception

Files: m4/ltoptions.m4
 m4/ltsugar.m4
 m4/lt~obsolete.m4
Copyright: 2004, 2005, 2007-2009, 2011-2015, Free Software
License: FSFULLR

Files: m4/nls.m4
Copyright: 1995-2003, 2005, 2006, 2008-2014, 2016, 2019, 2020, Free
License: FSFULLR and/or GPL and/or LGPL

Files: src/*
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/client/plymouth.c
Copyright: 2007, Red Hat, Inc
License: GPL-2+

Files: src/libply-splash-graphics/ply-image.c
Copyright: 2006, 2007, Red Hat, Inc.
 2003, University of Southern California
License: GPL-2+

Files: src/libply/ply-bitarray.c
 src/libply/ply-bitarray.h
 src/libply/ply-hashtable.c
 src/libply/ply-hashtable.h
Copyright: 2009, Charlie Brej <cbrej@cs.man.ac.uk>
License: GPL-2+

Files: src/libply/ply-buffer.h
 src/libply/ply-logger.h
Copyright: 2007, 2008, Ray Strode <rstrode@redhat.com>
License: GPL-2+

Files: src/libply/ply-rectangle.c
 src/libply/ply-region.c
Copyright: 2009, Red Hat, Inc.
 2009, Charlie Brej <cbrej@cs.man.ac.uk>
License: GPL-2+

Files: src/main.c
Copyright: 2007, Red Hat, Inc
License: GPL-2+

Files: src/plugins/controls/label/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/renderers/drm/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/renderers/frame-buffer/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/renderers/x11/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/splash/details/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/splash/fade-throbber/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/splash/script/*
Copyright: 2009, Charlie Brej <cbrej@cs.man.ac.uk>
License: GPL-2+

Files: src/plugins/splash/script/plugin.c
 src/plugins/splash/script/plugin.h
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/splash/space-flares/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/splash/text/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/splash/tribar/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/plugins/splash/two-step/plugin.c
Copyright: 2006-2019, Red Hat, Inc.
License: GPL-2+

Files: src/upstart-bridge/*
Copyright: 2010, 2011, Canonical Ltd.
License: GPL-2+

Files: * src/Makefile.in src/client/Makefile.in src/libply-splash-core/Makefile.in src/libply-splash-graphics/Makefile.in src/libply/Makefile.in src/plugins/* src/plugins/splash/script/Makefile.in src/upstart-bridge/Makefile.in
Copyright: 2006-2008 Red Hat, Inc.
 2007-2008 Ray Strode <halfline@gmail.com>
 2003 University of Southern California
 2003 Charlie Brej <cbrej@cs.man.ac.uk>
License: GPL-2+
